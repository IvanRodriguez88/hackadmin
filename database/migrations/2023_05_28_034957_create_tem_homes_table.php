<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tem_homes', function (Blueprint $table) {
            $table->id();
			$table->string('title');
			$table->text('description')->nullable();
			$table->string('image')->nullable();
			$table->boolean('is_active')->default(1);

			//Datos de creación y modificación
			$table->string('notes', 1024)->nullable()->comment('Notas');
			$table->smallInteger('created_by')->unsigned()->default(1)->comment('Usuario que creó');
			$table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
			$table->smallInteger('updated_by')->unsigned()->default(1)->comment('Último usuario que modificó');
			$table->foreign('updated_by')->references('id')->on('users')->onDelete('restrict');
			$table->timestamp('created_at', 0)->useCurrent()->comment('Fecha de creación');
			$table->timestamp('updated_at', 0)->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
				->comment('Última fecha de modificación');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tem_homes');
    }
};
