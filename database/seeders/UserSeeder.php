<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		User::create([
			"name" => 'Hackathon',
			"paternal_surname" => 'Banorte',
			"maternal_surname" => '2023',
			"email" => 'hackathon@banorte.com',
			"password" => bcrypt('banorte'),
			"role_id" => 1,
			"created_at" => date("Y-m-d H:i:s"),
			"updated_at" => date("Y-m-d H:i:s"),
		]);
		
	}
}
