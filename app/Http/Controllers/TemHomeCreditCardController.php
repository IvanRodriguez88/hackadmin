<?php

namespace App\Http\Controllers;

use App\Models\TemHomeCreditCard;

use App\Http\Requests\TemHomeCreditCardRequest;
use App\DataTables\TemHomeCreditCardDataTable;
use Illuminate\Http\Request;
use App\Traits\UploadFilesTrait;

class TemHomeCreditCardController extends Controller
{
	use UploadFilesTrait;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//Consultar permiso para botón de agregar
		$allowAdd = auth()->user()->hasPermissions("tem_home_credit_cards.create");
		$allowEdit = auth()->user()->hasPermissions("tem_home_credit_cards.edit");
		return (new TemHomeCreditCardDataTable())->render('tem-home-credit-cards.index', compact('allowAdd', 'allowEdit'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('tem-home-credit-cards.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(TemHomeCreditCardRequest $request)
	{
		$status = true;
		$tem_home_credit_card = null;
		$params = array_merge($request->all(), [
			'created_by' => auth()->id(),
			'updated_by' => auth()->id(),
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		]);
		if(request()->file('image') != null){
			$path = $this->uploadFile(request()->file('image'), 'uploads/banners');
			$params['image'] = $path;
		}
		try {
			$tem_home_credit_card = TemHomeCreditCard::create($params);
			$message = __('tem_home_credit_cards.Successfully created');
		} catch (\Illuminate\Database\QueryException $e) {
			$status = false;
			$message = $this->getErrorMessage($e, 'tem_home_credit_cards');
		}
		return $this->getResponse($status, $message, $tem_home_credit_card);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\TemHomeCreditCard  $tem_home_credit_card
	 * @return \Illuminate\Http\Response
	 */
	public function show(TemHomeCreditCard $tem_home_credit_card)
	{
		return view('tem-home-credit-cards.show', compact('tem_home_credit_card'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\TemHomeCreditCard  $tem_home_credit_card
	 * @return \Illuminate\Http\Response
	 */
	public function edit(TemHomeCreditCard $tem_home_credit_card)
	{
		return view('tem-home-credit-cards.edit', compact('tem_home_credit_card'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\TemHomeCreditCard  $tem_home_credit_card
	 * @return \Illuminate\Http\Response
	 */
	public function update(TemHomeCreditCardRequest $request, TemHomeCreditCard $tem_home_credit_card)
	{
		$status = true;
		$params = array_merge($request->all(), [
			'updated_by' => auth()->id(),
			'updated_at' => date("Y-m-d H:i:s")
		]);
		if(request()->file('image') != null){
			$path = $this->uploadFile(request()->file('image'), 'uploads/banners');
			$params['image'] = $path;
		}
		try {
			$tem_home_credit_card->update($params);
			$message = __('tem_home_credit_cards.Successfully updated');
		} catch (\Illuminate\Database\QueryException $e) {
			$status = false;
			$message = $this->getErrorMessage($e, 'tem_home_credit_cards');
		}
		return $this->getResponse($status, $message, $tem_home_credit_card);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\TemHomeCreditCard  $tem_home_credit_card
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(TemHomeCreditCard $tem_home_credit_card)
	{
		$status = true;
		try {
			$tem_home_credit_card->delete();
			$message = __('tem_home_credit_cards.Successfully deleted');
		} catch (\Illuminate\Database\QueryException $e) {
			$status = false;
			$message = $this->getErrorMessage($e, 'tem_home_credit_cards');
		}
		return $this->getResponse($status, $message);
	}

	public function getQuickModalContent(TemHomeCreditCard $tem_home_credit_card = null)
	{
		$params = request("params");
		return response()->json(view('crud-maker.components.modal-quickadd', compact('params', 'tem_home_credit_card'))->render());
	}
}
