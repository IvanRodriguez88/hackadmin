<?php

namespace App\Http\Controllers;

use App\Models\TemHome;

use App\Http\Requests\TemHomeRequest;
use App\DataTables\TemHomeDataTable;
use Illuminate\Http\Request;
use App\Traits\UploadFilesTrait;

class TemHomeController extends Controller
{
	use UploadFilesTrait;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//Consultar permiso para botón de agregar
		$allowAdd = auth()->user()->hasPermissions("tem_homes.create");
		$allowEdit = auth()->user()->hasPermissions("tem_homes.edit");
		return (new TemHomeDataTable())->render('tem-homes.index', compact('allowAdd', 'allowEdit'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('tem-homes.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(TemHomeRequest $request)
	{
		$status = true;
		$tem_home = null;
		$params = array_merge($request->all(), [
			'created_by' => auth()->id(),
			'updated_by' => auth()->id(),
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		]);
		if(request()->file('image') != null){
			$path = $this->uploadFile(request()->file('image'), 'uploads/banners');
			$params['image'] = $path;
		}
		try {
			$tem_home = TemHome::create($params);
			$message = __('tem_homes.Successfully created');
		} catch (\Illuminate\Database\QueryException $e) {
			$status = false;
			$message = $this->getErrorMessage($e, 'tem_homes');
		}
		return $this->getResponse($status, $message, $tem_home);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\TemHome  $tem_home
	 * @return \Illuminate\Http\Response
	 */
	public function show(TemHome $tem_home)
	{
		return view('tem-homes.show', compact('tem_home'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\TemHome  $tem_home
	 * @return \Illuminate\Http\Response
	 */
	public function edit(TemHome $tem_home)
	{
		return view('tem-homes.edit', compact('tem_home'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\TemHome  $tem_home
	 * @return \Illuminate\Http\Response
	 */
	public function update(TemHomeRequest $request, TemHome $tem_home)
	{
		$status = true;
		$params = array_merge($request->all(), [
			'updated_by' => auth()->id(),
			'updated_at' => date("Y-m-d H:i:s")
		]);
		if(request()->file('image') != null){
			$path = $this->uploadFile(request()->file('image'), 'uploads/banners');
			$params['image'] = $path;
		}
		try {
			$tem_home->update($params);
			$message = __('tem_homes.Successfully updated');
		} catch (\Illuminate\Database\QueryException $e) {
			$status = false;
			$message = $this->getErrorMessage($e, 'tem_homes');
		}
		return $this->getResponse($status, $message, $tem_home);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\TemHome  $tem_home
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(TemHome $tem_home)
	{
		$status = true;
		try {
			$tem_home->delete();
			$message = __('tem_homes.Successfully deleted');
		} catch (\Illuminate\Database\QueryException $e) {
			$status = false;
			$message = $this->getErrorMessage($e, 'tem_homes');
		}
		return $this->getResponse($status, $message);
	}

	public function getQuickModalContent(TemHome $tem_home = null)
	{
		$params = request("params");
		return response()->json(view('crud-maker.components.modal-quickadd', compact('params', 'tem_home'))->render());
	}
}
