<?php

namespace App\DataTables;

use App\Models\TemHomeCreditCard;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TemHomeCreditCardDataTable extends BlankDataTable
{
	public function __construct()
	{
		$this->routeResource = 'tem_home_credit_cards';
	}
	/**
	 * Get query source of dataTable.
	 *
	 * @param \App\Selling $model
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(TemHomeCreditCard $model)
	{
		return $model
		->newQuery();
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		$pColumns = parent::getColumns();
		$columns = [
			['data' => 'id', 'visible' => false, 'title' => __('tem_home_credit_cards.id')],
			['data' => 'name', 'title' => __('tem_home_credit_cards.name')],
			['data' => 'image', 'title' => __('tem_home_credit_cards.image')],
			['data' => 'url', 'title' => __('tem_home_credit_cards.url')],
			['data' => 'is_active', 'title' => __('tem_home_credit_cards.is_active')],
			['data' => 'notes', 'title' => __('tem_home_credit_cards.notes')],
			['data' => 'created_by', 'visible' => false, 'title' => __('tem_home_credit_cards.created_by')],
			['data' => 'updated_by', 'visible' => false, 'title' => __('tem_home_credit_cards.updated_by')],
			['data' => 'created_at', 'visible' => false, 'title' => __('tem_home_credit_cards.created_at')],
			['data' => 'updated_at', 'visible' => false, 'title' => __('tem_home_credit_cards.updated_at')],
		];
		return array_merge($columns, $pColumns);
	}

	public function dataTable($query)
	{
		$datatable = parent::dataTable($query);
		$datatable->editColumn('image', function ($row) {
			$url= asset($row->image);
    		return '<img src="'.$url.'" border="0" height="50" class="img-rounded" align="center" />';
		})->rawColumns(["is_active", "image", "action"]);
		
		return $datatable;
	}
}
