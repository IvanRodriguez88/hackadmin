<?php

namespace App\DataTables;

use App\Models\TemHome;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TemHomeDataTable extends BlankDataTable
{
	public function __construct()
	{
		$this->routeResource = 'tem_homes';
	}
	/**
	 * Get query source of dataTable.
	 *
	 * @param \App\Selling $model
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(TemHome $model)
	{
		return $model
		->newQuery();
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		$pColumns = parent::getColumns();
		$columns = [
			['data' => 'id', 'visible' => false, 'title' => __('tem_homes.id')],
			['data' => 'title', 'title' => __('tem_homes.title')],
			['data' => 'description', 'title' => __('tem_homes.description')],
			['data' => 'image', 'title' => __('tem_homes.image')],
			['data' => 'is_active', 'title' => __('tem_homes.is_active')],
			['data' => 'notes', 'title' => __('tem_homes.notes')],
			['data' => 'created_by', 'visible' => false, 'title' => __('tem_homes.created_by')],
			['data' => 'updated_by', 'visible' => false, 'title' => __('tem_homes.updated_by')],
			['data' => 'created_at', 'visible' => false, 'title' => __('tem_homes.created_at')],
			['data' => 'updated_at', 'visible' => false, 'title' => __('tem_homes.updated_at')],
		];
		return array_merge($columns, $pColumns);
	}

	public function dataTable($query)
	{
		$datatable = parent::dataTable($query);
		$datatable->editColumn('image', function ($row) {
			$url= asset($row->image);
    		return '<img src="'.$url.'" border="0" height="50" class="img-rounded" align="center" />';
		})->rawColumns(["is_active", "image",'description', "action"]);
		
		return $datatable;
	}


}
