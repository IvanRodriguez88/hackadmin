<?php
namespace App\Traits;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait UploadFilesTrait
{
	public function uploadFile($file, $path = null, $name = null, $type = 'file', $disk = 'public')
	{
		$name = ($name ?? Str::random(25)).'.'.$file->getClientOriginalExtension();
		$result = $file->store($path, $disk);
		return $result;
	}
}