$(function() {
	var entity = $("#entity").val();

	window.customizeDatatable = () => {
		//Change filter section
		var filter = $(".dataTables_wrapper .dataTables_filter");
		filter.addClass('search-box me-2 mb-2 row');
		if(filter.find('.button-add').length == 0) {
			if($("#allowAdd").val() == "1") {
				filter.append(getAddButton());
			}
		}
		filter.find("label").addClass('position-relative col-4').append(`<i class="bx bx-search-alt search-icon"></i>`);
		filter.find("input[type=search]").addClass('form-control');
		
		//Change pagination links
		var paginate = $(".dataTables_wrapper .dataTables_paginate");
		paginate.addClass('pagination pagination-rounded justify-content-end mb-2');
		var icon = `
			<span class='page-item'>
				<span class='page-link'><i class='mdi mdi-chevron-left'></i></span>
			</span>
		`;
		paginate.find(".previous").html(addIcon('<i class="mdi mdi-chevron-left"></i>'));
		paginate.find(".next").html(addIcon('<i class="mdi mdi-chevron-right"></i>'));
		paginate.find("> span").css('display', 'inline-flex');
		paginate.find("span a").each(function(index, el) {
			var classList = el.classList;
			var active = false;
			for (const key in classList) {
				if(classList[key] == 'current') {
					active = true;
				}
			}
			$(el).html(addIcon($(el).html(), active))
		});
	}

	window.getAddButton = () => {
		var result = '';
		var params = btoa(JSON.stringify({
			"entity_source": entity,
			"entity": entity, //Este es el nombre que se le dará al quick add modal que se creará
			"saveAditionals": 'reloadDatatable'
		}));
		result = `
		<div class="col-sm-8">
			<div class="text-sm-end">
				<button type="button" class="btn btn-default btn-rounded waves-effect mb-2 button-add" onclick="showQuickAddModal('${params}')">
					<i class="mdi mdi-plus me-1"></i>
					${window.i18n.es.Add}
				</button>
			</div>
		</div>`;
		return result;
	}

	window.addIcon = (p, a) => {
		return `
			<span class='page-item `+(a ? 'active' : '')+`'>
				<span class='page-link'>${p}</span>
			</span>
		`;
	}

	window.reloadDatatable = (response) => {
		window.LaravelDataTables[entity+"-table"].draw();
	}
});