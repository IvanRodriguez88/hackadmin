@extends('crud-maker.layouts.index', [
	'title' => __('tem_homes.title_index'), 
	'entity' => 'tem_homes', 
	'form' => 'temHome',
])

@section('datatable')
<hr>
	<div class="row">
		<div class="col-md-4 p-2">
			<img class="img-fluid" src="{{asset('images/img2.png')}}" alt="">	
			<p class="font-size-24 p-2 text-center">Configura esta sección cambiando títulos, textos e imágenes del Inicio.</p>
		</div>
		<div class="col-md-8 p-2">
			{{ $dataTable->table(["width" => "100%"]) }}
		</div>
	</div>
@endsection