@extends('layouts.app', [
	'title' => __('tem_homes.title_show'), 
])

@section('content')
<div class="report-container p-3">
	<div class="row">
		<div class="col-sm-12 col-md-6 offset-md-3">
			<br>
			<div class="card card-info">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-md-6 card-title">@lang('tem_homes.title_show')</div>
						<div class="col-sm-12 col-md-6 text-right">
							<a href="{{ route('tem_homes.index') }}">
								<i class="fas fa-long-arrow-alt-left"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.id')</div>
						<div class="col-sm-6">{{ $tem_home->id }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.title')</div>
						<div class="col-sm-6">{{ $tem_home->title }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.description')</div>
						<div class="col-sm-6">{{ $tem_home->description }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.image')</div>
						<div class="col-sm-6">{{ $tem_home->image }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.is_active')</div>
						<div class="col-sm-6">{{ $tem_home->is_active }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.notes')</div>
						<div class="col-sm-6">{{ $tem_home->notes }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.created_by')</div>
						<div class="col-sm-6">{{ $tem_home->created_by }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.updated_by')</div>
						<div class="col-sm-6">{{ $tem_home->updated_by }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.created_at')</div>
						<div class="col-sm-6">{{ $tem_home->created_at }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_homes.updated_at')</div>
						<div class="col-sm-6">{{ $tem_home->updated_at }}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection