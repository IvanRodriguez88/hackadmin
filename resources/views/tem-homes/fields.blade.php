@include("crud-maker.components.form-row", ["params" => [
	[
		"name" => "title",
		"id" => "title",
		"class" => "form-control",
		"entity" => "tem_homes",
		"type" => "text",
		"defaultValue" => old("title") ?? ($tem_home->title ?? ""),
		"required" => "true",
	]
]])
@include("crud-maker.components.form-row", ["params" => [
	[
		"name" => "description",
		"id" => "description",
		"class" => "form-control",
		"entity" => "tem_homes",
		"type" => "textarea",
		"defaultValue" => old("description") ?? ($tem_home->description ?? ""),
		"rows" => "4",
	]
]])
	
<div class="row mb-3">
	<div class="col-12">
		<label for="image" class="form-label">@lang('tem_homes.image')</label>
		<input id="image" type="file" class="form-control" name="image" accept="image/*">
	</div>
</div>
@include("crud-maker.components.form-row", ["params" => [
		[
			"name" => "is_active",
			"id" => "is_active",
			"class" => "form-control",
			"entity" => "tem_homes",
			"type" => "select",
			"defaultValue" => old("is_active") ?? ($tem_home->is_active ?? ""),
			"required" => "true",
			"elements" => ['1' => "Si", '0' => "No"],
		]
]])
@include("crud-maker.components.form-row", ["params" => [
	[
		"name" => "notes",
		"id" => "notes",
		"class" => "form-control",
		"entity" => "tem_homes",
		"type" => "textarea",
		"defaultValue" => old("notes") ?? ($tem_home->notes ?? ""),
	]
]])


<script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
<script>
	$(function() {
		tinymce.init({
			selector: '#description',
			height: "250",
			setup: function (editor) {
				editor.on('change', function () {
					tinymce.triggerSave();
				});
			}
		});	
	});
</script>