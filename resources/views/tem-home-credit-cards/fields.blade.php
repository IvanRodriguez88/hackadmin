@include("crud-maker.components.form-row", ["params" => [
	[
		"name" => "name",
		"id" => "name",
		"class" => "form-control",
		"entity" => "tem_home_credit_cards",
		"type" => "text",
		"defaultValue" => old("name") ?? ($tem_home_credit_card->name ?? ""),
		"required" => "true",
	]
]])
<div class="row mb-3">
	<div class="col-12">
		<label for="image" class="form-label">@lang('tem_homes.image')</label>
		<input id="image" type="file" class="form-control" name="image" accept="image/*">
	</div>
</div>
@include("crud-maker.components.form-row", ["params" => [
	[
		"name" => "url",
		"id" => "url",
		"class" => "form-control",
		"entity" => "tem_home_credit_cards",
		"type" => "text",
		"defaultValue" => old("url") ?? ($tem_home_credit_card->url ?? ""),
	]
]])
@include("crud-maker.components.form-row", ["params" => [
		[
			"name" => "is_active",
			"id" => "is_active",
			"class" => "form-control",
			"entity" => "tem_homes",
			"type" => "select",
			"defaultValue" => old("is_active") ?? ($tem_home_credit_cards->is_active ?? ""),
			"required" => "true",
			"elements" => ['1' => "Si", '0' => "No"],
		]
]])
@include("crud-maker.components.form-row", ["params" => [
	[
		"name" => "notes",
		"id" => "notes",
		"class" => "form-control",
		"entity" => "tem_home_credit_cards",
		"type" => "textarea",
		"defaultValue" => old("notes") ?? ($tem_home_credit_card->notes ?? ""),
	]
]])
