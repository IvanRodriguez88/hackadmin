@extends('layouts.app', [
	'title' => __('tem_home_credit_cards.title_show'), 
])

@section('content')
<div class="report-container p-3">
	<div class="row">
		<div class="col-sm-12 col-md-6 offset-md-3">
			<br>
			<div class="card card-info">
				<div class="card-header">
					<div class="row">
						<div class="col-sm-12 col-md-6 card-title">@lang('tem_home_credit_cards.title_show')</div>
						<div class="col-sm-12 col-md-6 text-right">
							<a href="{{ route('tem_home_credit_cards.index') }}">
								<i class="fas fa-long-arrow-alt-left"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.id')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->id }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.name')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->name }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.image')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->image }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.url')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->url }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.is_active')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->is_active }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.notes')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->notes }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.created_by')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->created_by }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.updated_by')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->updated_by }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.created_at')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->created_at }}</div>
					</div>
					<div class="row">
						<div class="col-sm-4 offset-sm-1 text-end">@lang('tem_home_credit_cards.updated_at')</div>
						<div class="col-sm-6">{{ $tem_home_credit_card->updated_at }}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection