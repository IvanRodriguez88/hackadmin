@extends('crud-maker.layouts.index', [
	'title' => __('tem_home_credit_cards.title_index'), 
	'entity' => 'tem_home_credit_cards', 
	'form' => 'temHomeCreditCard',
])

@section('datatable')
	<hr>
	<div class="row">
		<div class="col-md-4 p-2">
			<img class="img-fluid" src="{{asset('images/img1.png')}}" alt="">	
			<p class="font-size-24 p-2 text-center">Configura esta sección cambiando las posibles tarjetas disponibles mostradas en el menú principal.</p>
		</div>
		<div class="col-md-8 p-2">
			{{ $dataTable->table(["width" => "100%"]) }}
		</div>
	</div>
@endsection