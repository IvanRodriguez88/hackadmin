@extends('layouts.login')

@section('header')
	<h4 class="pb-1">
		@lang('auth.login_title')
	</h4>
	<h6 class="pb-2">
		@lang('auth.login_subtitle')
	</h6>
@endsection
@section('body')
	@include('auth.login-fields')
@endsection
