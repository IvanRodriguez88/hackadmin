<?php
return [
	//Titles
	"title_index" => "Configuracion de Inicio",
	"title_add" => "Agregar Inicio",
	"title_show" => "Ver Inicio",
	"title_edit" => "Modificar Inicio",
	"title_delete" => "Eliminar Inicio",

	//Fields
	"id" => "id",
	"title" => "Título",
	"description" => "Descripción",
	"image" => "Imagen",
	"is_active" => "Activa",
	"notes" => "Notas",
	"created_by" => "Creado por",
	"updated_by" => "Modificado por",
	"created_at" => "Fecha creado",
	"updated_at" => "Fecha modificado",

	//Action messages
	"confirm_delete" => "Se borrará configuración de inicio de la base de datos. ¿Desea continuar?",
	"Successfully created" => "configuración de inicio creado correctamente",
	"Successfully updated" => "configuración de inicio modificado correctamente",
	"Successfully deleted" => "configuración de inicio eliminado correctamente",
	"delete_error_message" => "Error al intentar eliminar configuración de inicio de la base de datos",
	"delete_error_message_constraint" => "No se puede eliminar configuración de inicio, hay tablas que dependen de este",
];