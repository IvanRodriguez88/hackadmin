<?php
return [
	//Titles
	"title_index" => "Configuración de tarjetas",
	"title_add" => "Agregar tarjeta",
	"title_show" => "Ver tarjeta",
	"title_edit" => "Modificar tarjeta",
	"title_delete" => "Eliminar tarjeta",

	//Fields
	"id" => "id",
	"name" => "Nombre",
	"image" => "Imagen",
	"url" => "Redirecciona a",
	"is_active" => "Activa",
	"notes" => "Notas",
	"created_by" => "Creado por",
	"updated_by" => "Modificado por",
	"created_at" => "Fecha creado",
	"updated_at" => "Fecha modificado",

	//Action messages
	"confirm_delete" => "Se borrará tarjeta de la base de datos. ¿Desea continuar?",
	"Successfully created" => "tarjeta creado correctamente",
	"Successfully updated" => "tarjeta modificado correctamente",
	"Successfully deleted" => "tarjeta eliminado correctamente",
	"delete_error_message" => "Error al intentar eliminar tarjeta de la base de datos",
	"delete_error_message_constraint" => "No se puede eliminar tarjeta, hay tablas que dependen de este",
];